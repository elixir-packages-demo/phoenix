# [phoenix](https://hex.pm/packages/phoenix)

![Hex.pm](https://img.shields.io/hexpm/dw/phoenix)
Productive. Reliable. Fast. Web framework

# Unofficial documentation
* [*Introduction to Phoenix*
  ](https://serokell.io/blog/introduction-to-phoenix)
  2021-06 Gints Dreimanis
* [*Static Site Generator using Elixir and Phoenix*
  ](https://amattn.com/p/static_site_generator_using_elixir_and_phoenix.html)
  2021-01 amattn
* (video 1h) [*Phoenix: an Intro to Elixir's Web Framework*
  ](https://www.youtube.com/watch?v=F-7MX_Az6_4)
  2017-07 Sonny Scroggi
* (video 1h) [*Phoenix a Web Framework for the New Web*
  ](https://www.youtube.com/watch?v=bk3icU8iIto)
  2016-11 José Valim
